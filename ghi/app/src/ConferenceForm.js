import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    // getting the list of conference location
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    // the actual location for selection
    const [location, setLocation] = useState('');



    // create a new method named handleNameChange that handles text input
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    // handle submit form
    const handleSubmit = async (event) => {
        event.preventDefault();
        // create an empty JSON object
        const data = {};
        // Assign the form data to the JSON object
        // the left side name must match the model in Insomina in create
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }


    }



    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input placeholder="Name" onChange={handleNameChange} required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="start" onChange={handleStartsChange} required type="date" name="starts" id="start" className="form-control" />
                            <label htmlFor="starts">starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="end" onChange={handleEndsChange} required type="date" name="ends" id="end" className="form-control" />
                            <label htmlFor="ends">ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">Description</label>
                            <textarea onChange={handleDescriptionChange} className="form-control" name="description" id="description" rows="4"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Max Presentations" onChange={handleMaxPresentationsChange} required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                            <label htmlFor="max_presentations">Maximum Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Max Attendees" onChange={handleMaxAttendeesChange} required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                            <label htmlFor="max_presentations">Maximum Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} required id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ConferenceForm;
